/*
 * Copyright (C) 2010 Michael Pardo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Data-Structures-In-Java
 * ArrayUtils.java
 */

package com.activeharmony.util;

import java.lang.reflect.Array;

/**
 * Utilities for Arrays
 */
public class ArrayUtils {
    /**
     * Method to check if a given array is empty
     *
     * @param array array
     * @return boolean empty status of array
     */
    public static <T> boolean isEmpty(T[] array) {
        return array == null || array.length == 0;
    }

    /**
     * Method to convert array to string
     *
     * @param array array
     * @return {@link String}
     */
    public static String toString(String[] array) {
        return String.join(",", array);
    }

    /**
     * Method to convert stream of items to array
     *
     * @param items items
     * @return {@link T[]}
     */
    @SafeVarargs
    public static <T> T[] toArray(final T... items) {
        return items;
    }

    /**
     * Method to clone a array
     *
     * @param array array
     * @return {@link T[]}
     */
    public static <T> T[] clone(final T[] array) {
        if (array == null) {
            return null;
        }
        return array.clone();
    }

    /**
     * Method to check if two arrays are of same length
     *
     * @param array1 array1
     * @param array2 array2
     * @return {@link boolean}
     */
    public static boolean isSameLength(final Object[] array1, final Object[] array2) {
        return getLength(array1) == getLength(array2);
    }

    /**
     * Method to get length of the array
     *
     * @param array array
     * @return {@link int}
     */
    public static int getLength(final Object array) {
        if (array == null) {
            return 0;
        }
        return Array.getLength(array);
    }

    /**
     * Method to check if two arrays are of same type
     *
     * @param array1 array1
     * @param array2 array2
     * @return {@link boolean}
     */
    public static boolean isSameType(final Object array1, final Object array2) {
        if (array1 == null || array2 == null) {
            throw new IllegalArgumentException("The Array must not be null");
        }
        return array1.getClass().getName().equals(array2.getClass().getName());
    }

    /**
     * Method to do a binary search on int array
     *
     * @param array array
     * @param size size
     * @param value vlaue
     * @return {@link int}
     */
    public static int binarySearch(int[] array, int size, int value) {
        int lower = 0;
        int higher = size - 1;
        while (lower <= higher) {
            final int mid = (lower + higher) >>> 1;
            final int midVal = array[mid];
            if (midVal < value) {
                lower = mid + 1;
            } else if (midVal > value) {
                higher = mid - 1;
            } else {
                return mid;
            }
            }
        return ~lower;
    }

}