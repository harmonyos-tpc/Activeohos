# Activeohos

Activeohos : Activeohos is an active record style ORM (object relational mapper). What does that mean exactly? Well, Activeohos allows you to save and retrieve SQLite database records without ever writing a single SQL statement. Each database record is wrapped neatly into a class with methods like save() and delete().

Activeohos does so much more than this though. Accessing the database is a hassle, to say the least. Activeohos takes care of all the setup and messy stuff, and all with just a few simple steps of configuration.

# Activeohos includes :
* Inserting, Deleting and updating database.
* Setting up the database.
* Create required database and perform operations on it.
* Raw queries, Delete queries.
* Database Encryption.

# Usage Instructions

1. The following core classes are the essential interface to Activeohos:
	ActiveHarmony - Hepls in initializing the database with given configuration
	Annotation: Manages resolving the @column, @Table annotation
	Model - Defines the methods for creating, managing the database

2. The steps to initialize the database and Activeohos:
	-> make MyApplication(it will change as per your application name) to extends Application class
		eg: public class MyApplication extends Application {} //This helps in initialization and dispose of the ActiveHarmony

	-> Load libsqlcipher.so
    
		SQLiteDatabase.loadLibs(this);
        
		NOTE: Keep application name to "MyApplication"(libsqlcipher.so dependency)
	
	-> Create Class which extends Model class and define the required columns in table.
		For example:
        
		@Table(name = "Details", id = "_id")
		public class Details extends Model {
		@Column(name = "Age")
		private String age;

		@Column(name = "id")
		private long id;
		}
		
	-> Create new Configuration.Builder object, add the required Model class and set the package name of the sample app
    
		Configuration.Builder configurationBuilder = new Configuration.Builder(this);
        configurationBuilder.addModelClasses(Details.class);
        configurationBuilder.setPackageName("com.activeharmony.sample"); // use your sample app package name
        ActiveHarmony.initialize(configurationBuilder.create());
		
3. Please refer the sample app for the CURD operation's

# Installation Instructions

1. For using Activeohos module in your sample application, add below dependencies in "entry" module to generate hap/har:

	Modify entry build.gradle as below :
    
	```
	dependencies {
    	implementation project(path: ':library')
		implementation 'io.openharmony.tpc.thirdlib:ohos-database-sqlcipher:1.0.2'
	}
	```

2. For using Activeohos in separate application, add the below dependencies and include "Activeohos.har" in libs folder of "entry" module  :

	Modify entry build.gradle as below :
    
    ```
	dependencies {
		implementation fileTree(dir: 'libs', include: ['*.har'])
		implementation 'io.openharmony.tpc.thirdlib:ohos-database-sqlcipher:1.0.2'
	}
    ```


3. For using Activeohos from a remote repository in separate application, add the below dependencies  :
Modify entry build.gradle as below :
        
	```
    dependencies {
        	implementation 'io.openharmony.tpc.thirdlib:Activeohos:1.0.0'
            implementation 'io.openharmony.tpc.thirdlib:ohos-database-sqlcipher:1.0.2'
    }
	```

# License

[Apache Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html)

```
Copyright (C) 2010 Michael Pardo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```

