/*
 * Copyright (C) 2010 Michael Pardo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.activeharmony.sample;

import com.activeharmony.sample.slice.Details;
import com.activeharmony.sample.slice.MainAbilitySlice;

import com.activeharmony.ActiveHarmony;
import com.activeharmony.Configuration;

import com.activeharmony.sample.utils.Constants;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

import net.sqlcipher.database.SQLiteDatabase;

/**
 * MainAbility.
 */
public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        SQLiteDatabase.loadLibs(this);
        Configuration.Builder configurationBuilder = new Configuration.Builder(this);
        configurationBuilder.addModelClasses(Details.class);
        configurationBuilder.setPackageName(Constants.PACKAGE_NAME);
        ActiveHarmony.initialize(configurationBuilder.create());
        super.setMainRoute(MainAbilitySlice.class.getName());
    }
}